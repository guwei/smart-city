package com.sc.common.entity.admin.distributedfile;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-03-08 15:25:30
 * @description:
 */
public class SysDistributedFileSearch extends SysDistributedFile {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}