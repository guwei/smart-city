package com.sc.common.dto;

import com.sc.common.enums.ReturnCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "其他接口返回对象")
public class ApiResponseDto<T> {
    @ApiModelProperty(value = "返回码",name = "code",example = "1000")
    private String code;

    @ApiModelProperty(value = "返回消息",name = "message",example = "参数空")
    private String message;

    @ApiModelProperty(value = "返回数据",name = "data",example = "{}")
    private T data;

    public ApiResponseDto() {
        this.code = ReturnCode.SUCCESS.getCode();
        this.message = ReturnCode.SUCCESS.getMessage();
    }

    public ApiResponseDto(ReturnCode returnCode) {
        this.code = returnCode.getCode();
        this.message = returnCode.getMessage();
    }

    public ApiResponseDto(ReturnCode returnCode, T data) {
        this.code = returnCode.getCode();
        this.message = returnCode.getMessage();
        this.data = data;
    }

    public static ApiResponseDto success(){
        ApiResponseDto apiResponseDto = new ApiResponseDto(ReturnCode.SUCCESS);
        return apiResponseDto;
    }

    public static ApiResponseDto success(Object data){
        ApiResponseDto apiResponseDto = new ApiResponseDto(ReturnCode.SUCCESS,data);
        return apiResponseDto;
    }

    public static ApiResponseDto failure(ReturnCode returnCode){
        ApiResponseDto apiResponseDto = new ApiResponseDto(returnCode);
        return apiResponseDto;
    }

    public static ApiResponseDto failure(ReturnCode returnCode,Object data){
        ApiResponseDto apiResponseDto = new ApiResponseDto(returnCode,data);
        return apiResponseDto;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
