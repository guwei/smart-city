package com.sc.common.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class HtmlFilterUtil {
    /**
     * 默认使用relaxed()
     * 白名单标签
     */
    private Whitelist whiteList;


    /**
     * 配置过滤化参数,不对代码进行格式化
     */
    private Document.OutputSettings outputSettings;


    private HtmlFilterUtil() {
    }

    /**
     * 静态创建HtmlFilter方法
     *
     * @param whiteList 白名单标签
     * @param pretty    是否格式化
     * @return HtmlFilter
     */
    public static HtmlFilterUtil create(Whitelist whiteList, boolean pretty) {
        HtmlFilterUtil htmlFilterUtil = new HtmlFilterUtil();
        if (whiteList == null) {
            htmlFilterUtil.whiteList = Whitelist.relaxed();
        }
        htmlFilterUtil.outputSettings = new Document.OutputSettings().prettyPrint(pretty);
        return htmlFilterUtil;
    }

    /**
     * 静态创建HtmlFilter方法
     *
     * @return HtmlFilter
     */
    public static HtmlFilterUtil create() {
        return create(null, false);
    }

    /**
     * 静态创建HtmlFilter方法
     *
     * @param whiteList 白名单标签
     * @return HtmlFilter
     */
    public static HtmlFilterUtil create(Whitelist whiteList) {
        return create(whiteList, false);
    }

    /**
     * 静态创建HtmlFilter方法
     *
     * @param excludeTags 例外的特定标签
     * @param includeTags 需要过滤的特定标签
     * @param pretty      是否格式化
     * @return HtmlFilter
     */
    public static HtmlFilterUtil create(List<String> excludeTags, List<String> includeTags, boolean pretty) {
        HtmlFilterUtil filter = create(null, pretty);
        //要过滤的标签
        if (includeTags != null && !includeTags.isEmpty()) {
            String[] tags = (String[]) includeTags.toArray(new String[0]);
            filter.whiteList.removeTags(tags);
        }
        //例外标签
        if (excludeTags != null && !excludeTags.isEmpty()) {
            String[] tags = (String[]) excludeTags.toArray(new String[0]);
            filter.whiteList.addTags(tags);
        }
        return filter;
    }

    /**
     * 静态创建HtmlFilter方法
     *
     * @param excludeTags 例外的特定标签
     * @param includeTags 需要过滤的特定标签
     * @return HtmlFilter
     */
    public static HtmlFilterUtil create(List<String> excludeTags, List<String> includeTags) {
        return create(includeTags, excludeTags, false);
    }

    /**
     * @param content 需要过滤内容
     * @return 过滤后的String
     */
    public String clean(String content) {
        return Jsoup.clean(content, "", this.whiteList, this.outputSettings);

    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String text = "<a href=\"http://www.baidu.com/a\" onclick=\"alert(1);\"></a><script>alert(0);</script><b style=\"xxx\" onclick=\"<script>alert(0);</script>\">abc</>";
        System.out.println(HtmlFilterUtil.create().clean(text));
    }
}
