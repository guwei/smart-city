package com.sc.common.entity.admin.customer;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2019-12-27 11:37:51
 * @description:
 */
public class SysCustomerSearch extends SysCustomer {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}