package com.sc.common.enums;

/**
 * Created by wust on 2019/5/27.
 */
public enum OperationLogEnum {
    Insert("新增"),
    Update("修改"),
    Delete("删除"),
    Search("查询"),
    Save("保存"),
    Download("下载"),
    Upload("上传"),
    Import("导入"),
    Export("导出"),
    Login("登录"),
    Logout("注销"),
    MODULE_COMMON("通用模块"),
    MODULE_ADMIN_SETTING("平台管理-通用设置"),
    MODULE_ADMIN_COMPANY("平台管理-公司模块"),
    MODULE_ADMIN_DATA_SOURCE("平台管理-数据源模块"),
    MODULE_ADMIN_DEPARTMENT("平台管理-部门模块"),
    MODULE_ADMIN_PROJECT("平台管理-项目模块"),
    MODULE_ADMIN_ROLE("平台管理-角色模块"),
    MODULE_ADMIN_USER("平台管理-用户模块"),
    MODULE_ADMIN_ACCOUNT("平台管理-账号模块"),
    MODULE_ADMIN_CUSTOMER("平台管理-客户模块"),
    MODULE_ADMIN_ORGANIZATION("平台管理-组织架构模块"),
    MODULE_ADMIN_LOOKUP("平台管理-数据字典模块"),
    MODULE_ADMIN_IMPORT_EXPORT("平台管理-导入导出模块"),
    MODULE_ADMIN_APP_TOKEN("平台管理-应用程序标识管理模块"),
    MODULE_ADMIN_NOTIFICATION("平台管理-通知管理模块"),
    MODULE_AUTOTASK_JOB("自动作业-job管理"),
    MODULE_DEMO("DEMO服务"),
    MODULE_WO_WORK_ORDER("工单服务");

    private String value;
    public String getValue() {
        return value;
    }
    OperationLogEnum(String value){
        this.value = value;
    }
}
