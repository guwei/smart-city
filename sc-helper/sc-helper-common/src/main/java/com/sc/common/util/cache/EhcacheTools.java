/**
 * Created by wust on 2020-04-16 08:55:43
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.common.util.cache;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

/**
 * @author: wust
 * @date: Created in 2020-04-16 08:55:43
 * @description:
 *
 */
@Component
public class EhcacheTools {
    static Logger logger = LogManager.getLogger(EhcacheTools.class);

    @Autowired
    private CacheManager cacheManager;

    /**
     * 放入缓存，会覆盖原来的值
     * @param name
     * @param k
     * @param v
     * @return
     */
    public boolean put(String name, Object k, Object v){
        try {
            cacheManager.getCache(name).put(k, v);
        }catch (Throwable e){
            return false;
        }
        return true;
    }


    /**
     * key不存在则放入缓存
     * @param name
     * @param k
     * @param v
     * @return
     */
    public boolean putIfAbsent(String name, Object k, Object v){
        try {
            cacheManager.getCache(name).putIfAbsent(k, v);
        }catch (Throwable e){
            return false;
        }
        return true;
    }


    /**
     * 读取缓存
     * @param name 缓存名称
     * @param k key
     * @return
     */
    public Object get(String name, Object k){
        if(logger.isInfoEnabled()){
            logger.info("从本地缓存查询数据，name={},key={}",name,k);
        }
        try {
            if(cacheManager.getCache(name) != null){
                if(cacheManager.getCache(name).get(k) != null){
                    return cacheManager.getCache(name).get(k).get();
                }
            }
        }catch (Throwable e){
            if(logger.isInfoEnabled()){
                logger.error("从本地缓存查询数据出现异常",e);
            }
        }
        return null;
    }


    /**
     * 读取缓存
     * @param name 缓存名称
     * @param k key
     * @param clz 反序列化对象
     * @param <T>
     * @return
     */
    public <T> T get(String name, Object k, Class<T> clz){
        if(logger.isInfoEnabled()){
            logger.info("从本地缓存查询数据，name={},key={},clz={}",name,k,clz);
        }
        try {
            if(cacheManager.getCache(name) != null){
                if(cacheManager.getCache(name).get(k) != null){
                    return cacheManager.getCache(name).get(k,clz);
                }
            }
        }catch (Throwable e){
            if(logger.isInfoEnabled()){
                logger.error("从本地缓存查询数据出现异常",e);
            }
        }
        return null;
    }


    /**
     * 根据key回收缓存
     * @param name
     * @param k
     * @return
     */
    public boolean evict(String name,Object k){
        if(logger.isInfoEnabled()){
            logger.info("回收缓存，name={},key={}",name,k);
        }
        try {
        cacheManager.getCache(name).evict(k);
        }catch (Throwable e){
            if(logger.isInfoEnabled()){
                logger.error("回收缓存出现异常",e);
            }
            return false;
        }
        return true;
    }


    /**
     * key存在则回收缓存
     * @param name
     * @param k
     * @return
     */
    public boolean evictIfPresent(String name,Object k){
        if(logger.isInfoEnabled()){
            logger.info("回收缓存，name={},key={}",name,k);
        }
        try {
            cacheManager.getCache(name).evictIfPresent(k);
        }catch (Throwable e){
            if(logger.isInfoEnabled()){
                logger.error("回收缓存出现异常",e);
            }
            return false;
        }
        return true;
    }


    /**
     * 清空缓存
     * @param name
     * @return
     */
    public boolean clear(String name){
        if(logger.isInfoEnabled()){
            logger.info("清空缓存，name={}",name);
        }
        try {
            cacheManager.getCache(name).clear();
        }catch (Throwable e){
            if(logger.isInfoEnabled()){
                logger.error("清空缓存出现异常",e);
            }
            return false;
        }
        return true;
    }
}
