package com.sc.common.service;

import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import java.io.InputStream;
import java.util.Map;

public interface MinioStorageApiService {
    SysDistributedFile upload(String objectName, InputStream stream, Long size, Map<String, String> headerMap, Object sse, String contentType, String fileName, String fileType, String source, MinioStorageService minioStorageService);

    InputStream download(Long fileId, MinioStorageService minioStorageService);

    void delete(String bucketName, String objectName,MinioStorageService minioStorageService);

    String getObjectUrl(String bucketName,String objectName);

    String getPresignedObjectUrl(String bucketName,String objectName);
}
