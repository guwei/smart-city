package com.sc.common.entity.admin.mynotice;

import com.sc.common.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "sys_my_notice")
public class SysMyNotice extends BaseEntity {

    private static final long serialVersionUID = 4887695977324864065L;
    /**
     * 发布通知表sys_notification的id
     */
    @Column(name = "notification_id")
    private Long notificationId;

    private Long receiver;

    private String status;

    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    public Long getReceiver() {
        return receiver;
    }

    public void setReceiver(Long receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}