package com.sc.common.interceptors.context;


import com.sc.common.entity.admin.apptoken.SysAppToken;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.WebUtil;
import com.sc.common.util.cache.DataDictionaryUtil;

import java.util.Locale;

/**
 * @author ：wust
 * @date ：Created in 2019/8/8 10:47
 * @description：开放api请求走这里设置上下文
 * @version:
 */
public class OpenApiStrategy implements IStrategy{

    @Override
    public void setDefaultBusinessContext() {
        String localeStr = MyStringUtils.null2String(WebUtil.getHeader(ApplicationEnum.X_LOCALE.getStringValue()));
        if (MyStringUtils.isBlank(localeStr)) {
            DefaultBusinessContext.getContext().setLocale(WebUtil.getRequest().getLocale());
        }else{
            Locale locale = new Locale(localeStr.split("-")[0],localeStr.split("-")[1]);
            DefaultBusinessContext.getContext().setLocale(locale);
        }

        String appId = MyStringUtils.null2String(WebUtil.getHeader("appId"));
        SysAppToken appToken = DataDictionaryUtil.getAppTokenByAppId(appId);
        DefaultBusinessContext.getContext().setAgentId(appToken.getAgentId());
    }
}
