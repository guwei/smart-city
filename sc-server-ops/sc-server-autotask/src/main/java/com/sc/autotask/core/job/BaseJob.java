package com.sc.autotask.core.job;

import cn.hutool.core.convert.Convert;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.util.MyStringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.impl.JobDetailImpl;

/**
 * Created by wust on 2019/6/13.
 */
public abstract class BaseJob implements Job{
    @Override
    public void execute(JobExecutionContext jobExecutionContext){
        this.before(jobExecutionContext);
    }

    protected void before(JobExecutionContext jobExecutionContext){
        JobDetailImpl jobDetail = (JobDetailImpl) jobExecutionContext.getJobDetail();
        String jobName = jobDetail.getName();

        String projectIdStr = jobName.substring(0,jobName.indexOf("-"));
        if(MyStringUtils.isNotBlank(projectIdStr) && projectIdStr.length() == 19){
            Long projectId = Convert.toLong(projectIdStr);
            DefaultBusinessContext.getContext().setProjectId(projectId);
        }

        run(jobExecutionContext);
    }

    protected abstract void run(JobExecutionContext jobExecutionContext);
}
