package com.sc.admin.core.service;


import com.sc.common.entity.admin.account.resource.SysAccountResource;
import com.sc.common.service.BaseService;

/**
 * @author: wust
 * @date: 2020-03-26 09:45:35
 * @description:
 */
public interface SysAccountResourceService extends BaseService<SysAccountResource> {
}
