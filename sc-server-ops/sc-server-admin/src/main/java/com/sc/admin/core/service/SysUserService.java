package com.sc.admin.core.service;


import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.entity.admin.user.SysUserList;
import com.sc.common.entity.admin.userorganization.SysUserOrganizationSearch;
import com.sc.common.service.BaseService;
import java.util.List;


/**
 * Created by wust on 2019/4/18.
 */
public interface SysUserService extends BaseService<SysUser> {
    List<SysUserList> listPageByParentOrganization(SysUserOrganizationSearch search,int pageNum,int pageSize);
}
