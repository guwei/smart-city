package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.lookup.SysLookup;
import org.springframework.dao.DataAccessException;

/**
 * Created by wust on 2019/4/29.
 */
public interface SysLookupMapper extends IBaseMapper<SysLookup> {

    int deleteAll() throws DataAccessException;
}
