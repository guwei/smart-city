package com.sc.admin.core.xml.resolver;


import com.sc.admin.core.xml.XMLDefaultResolver;
import com.sc.common.entity.admin.menu.SysMenu;
import com.sc.common.entity.admin.resource.SysResource;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.MyStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;

public class XMLPermissionResolver extends XMLDefaultResolver {
    static Logger logger = LogManager.getLogger(XMLPermissionResolver.class);

    /**
     * 元素名称
     */

    private static final String ELEMENT_SUBSYSTEM = "subsystem";
    private static final String ELEMENT_PATH = "path";
    private static final String ELEMENT_MODULEGROUP = "modulegroup";
    private static final String ELEMENT_MODULE = "module";
    private static final String ELEMENT_OPERATION = "operation";

    /**
     * 属性名称
     */
    private static final String ELEMENT_ATTRIBUTE_USERTYPE = "userType";
    private static final String ELEMENT_ATTRIBUTE_CODE = "code";
    private static final String ELEMENT_ATTRIBUTE_NAME = "name";
    private static final String ELEMENT_ATTRIBUTE_DESC = "desc";
    private static final String ELEMENT_ATTRIBUTE_PERMISSION = "permission";
    private static final String ELEMENT_ATTRIBUTE_URL = "url";
    private static final String ELEMENT_ATTRIBUTE_REQUESTMETHOD = "requestMethod";
    private static final String ELEMENT_ATTRIBUTE_ORDER = "order";
    private static final String ELEMENT_ATTRIBUTE_IMG = "img";
    private static final String ELEMENT_ATTRIBUTE_PATH = "path";
    private static final String ELEMENT_ATTRIBUTE_PAGE = "page";
    private static final String ELEMENT_ATTRIBUTE_VISIBLE = "visible";





    /**
     * 解析到的数据集合
     */
    private final Map<String, Object> permissionsMap = new HashMap<>();
    private final Map<String, Object> parseMenuMap = new HashMap<>();
    private final Map<String, Object> parseResourceMap = new HashMap<>();
    private final List<SysMenu> parseMenuList = new ArrayList<SysMenu>();
    private final List<SysResource> parseResourceList = new ArrayList<SysResource>();


    @Override
    public Map<String, List> getResult() {
        this.parseXML();
        Map<String, List> map = new HashMap<>(2);
        map.put("parseMenuList",parseMenuList);
        map.put("parseResourceList",parseResourceList);
        return map;
    }

    public void parseXML() throws BusinessException {
        String mainXMLPath = "permissions" + File.separator + "xml" + File.separator + "main.xml";
        String mainXSDPath = "permissions" + File.separator + "xsd" + File.separator + "main.xsd";
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        validateXML(mainXSDPath, mainXMLPath);

        try{
            ClassPathResource resource = new ClassPathResource(mainXMLPath);

            DocumentBuilder db = dbf.newDocumentBuilder();
            org.w3c.dom.Document doc = db.parse(resource.getInputStream());
            org.w3c.dom.Element element = doc.getDocumentElement();
            doParseXML(element, db, "", "");
        }catch (Exception e){
            throw new BusinessException(e);
        }
    }


    private void doParseXML(org.w3c.dom.Element element, DocumentBuilder db, String userType, String lang) throws Exception {
        Node rootNode = element;
        if (rootNode.getNodeType() == Node.ELEMENT_NODE) {
            if (ELEMENT_SUBSYSTEM.equals(rootNode.getNodeName())) {   // 解析subsystem节点
                String code = element.getAttribute(ELEMENT_ATTRIBUTE_CODE);
                userType = element.getAttribute(ELEMENT_ATTRIBUTE_USERTYPE);
                String key = userType + "_" + lang + "_" + code;
                if (!parseMenuMap.containsKey(key)) {  // 忽略已经解析过的节点
                    if (!"*".equals(code) && StringUtils.isNotBlank(code)) {    // id属性值为星号或为空则可以忽略
                        String name = element.getAttribute(ELEMENT_ATTRIBUTE_NAME);
                        String desc = element.getAttribute(ELEMENT_ATTRIBUTE_DESC);
                        String url = element.getAttribute(ELEMENT_ATTRIBUTE_URL);
                        String orderString = element.getAttribute(ELEMENT_ATTRIBUTE_ORDER);
                        String order = StringUtils.isBlank(MyStringUtils.null2String(orderString)) ? "-1" : orderString;


                        SysMenu menuEntity = new SysMenu();
                        menuEntity.setCode(code);
                        menuEntity.setName(name);
                        menuEntity.setDescription(desc);
                        menuEntity.setUrl(url);
                        menuEntity.setLevel(0);
                        menuEntity.setSort(Long.valueOf(order));
                        menuEntity.setPcode(null);
                        menuEntity.setType("subsystem");
                        menuEntity.setIsParent("A100701");
                        menuEntity.setUserType(userType);
                        menuEntity.setLan(lang);
                        menuEntity.setCreateTime(new Date());
                        menuEntity.setIsDeleted(0);

                        parseMenuList.add(menuEntity);
                        parseMenuMap.put(key, menuEntity);
                    } else {
                        logger.info("属性code值为[" + code + "]，的节点忽略");
                    }
                } else {
                    logger.info("已经解析过此节点：" + parseMenuMap.get(key));
                }
            }
        }
        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            short nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                if (ELEMENT_PATH.equals(node.getNodeName())) {
                    String path = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_PATH);
                    if (!StringUtils.isBlank(path) && !"*".equals(path)) {// 星号表示可以忽略的节点
                        String XMLPath = path;
                       /* String XSDPath = "permissions" + File.separator + "xsd" + File.separator + "subsystem.xsd";
                        super.validateXML(XSDPath, XMLPath);*/
                        org.w3c.dom.Document doc = db.parse(new ClassPathResource(XMLPath).getInputStream());
                        org.w3c.dom.Element subElement = doc.getDocumentElement();

                        if(path.indexOf('_') != -1){
                            lang = path.substring(path.indexOf('_') + 1, path.indexOf('.'));
                        }else{
                            lang = "zh_CN";
                        }

                        doParseXML(subElement, db, userType, lang);
                    } else {
                        logger.info("属性path值为[" + path + "]，的节点忽略");
                    }
                }else if (ELEMENT_MODULEGROUP.equals(node.getNodeName())) {   // 解析modulegroup节点
                    String code = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_CODE);
                    String pcode = ((org.w3c.dom.Element) node.getParentNode()).getAttribute(ELEMENT_ATTRIBUTE_CODE);
                    String key = userType + "_" + lang + "_" + code;
                    String pkey = userType + "_" + lang + "_" + pcode;
                    if (!parseMenuMap.containsKey(key)) {  // 忽略已经解析过的节点
                        if (!"*".equals(code) && StringUtils.isNotBlank(code)) {    // id属性值为星号或为空则可以忽略
                            String name = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_NAME);
                            String desc = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_DESC);
                            String permission = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_PERMISSION);
                            String url = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_URL);
                            String orderString = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_ORDER);
                            String order = StringUtils.isBlank(MyStringUtils.null2String(orderString)) ? "-1" : orderString;
                            String img = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_IMG);
                            String visible = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_VISIBLE);
                            SysMenu sysMenuParent = parseMenuMap.get(pkey) == null ? null : (SysMenu) parseMenuMap.get(pkey);

                            SysMenu menuEntity = new SysMenu();
                            menuEntity.setCode(code);
                            menuEntity.setName(name);
                            menuEntity.setDescription(desc);
                            menuEntity.setPermission(permission);
                            menuEntity.setUrl(url);

                            menuEntity.setLevel(sysMenuParent == null ? 0 : (sysMenuParent.getLevel() + 1));
                            menuEntity.setSort(Long.valueOf(order));
                            menuEntity.setImg(img);
                            menuEntity.setPcode(pcode);
                            menuEntity.setType("modulegroup");
                            menuEntity.setVisible(visible);
                            menuEntity.setIsParent("A100701");
                            menuEntity.setUserType(userType);
                            menuEntity.setLan(lang);
                            menuEntity.setCreateTime(new Date());
                            menuEntity.setIsDeleted(0);

                            parseMenuList.add(menuEntity);
                            parseMenuMap.put(key, menuEntity);
                        } else {
                            logger.info("属性id值为[" + code + "]，的节点忽略");
                        }
                    } else {
                        logger.info("已经解析过此节点：" + parseMenuMap.get(key));
                    }
                } else if (ELEMENT_MODULE.equals(node.getNodeName())) {
                    String code = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_CODE);
                    String pcode = ((org.w3c.dom.Element) node.getParentNode()).getAttribute(ELEMENT_ATTRIBUTE_CODE);
                    String key = userType + "_" + lang + "_" + code;
                    String pkey = userType + "_" + lang + "_" + pcode;
                    if (!parseMenuMap.containsKey(key)) {  // 忽略已经解析过的节点
                        if (!"*".equals(code) && StringUtils.isNotBlank(code)) {    // id属性值为星号或为空则可以忽略
                            String name = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_NAME);
                            String desc = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_DESC);
                            String permission = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_PERMISSION);
                            String url = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_URL);
                            String orderString = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_ORDER);
                            String order = StringUtils.isBlank(MyStringUtils.null2String(orderString)) ? "-1" : orderString;
                            String img = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_IMG);
                            String page = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_PAGE);
                            String parentPage = ((org.w3c.dom.Element) node.getParentNode()).getAttribute(ELEMENT_ATTRIBUTE_PAGE);
                            String visible = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_VISIBLE);

                            SysMenu sysMenuParent = parseMenuMap.get(pkey) == null ? null : (SysMenu) parseMenuMap.get(pkey);

                            // 设置菜单权限标识，如果权限标识没有填写，则用page名称作为permission
                            String permissionNew = "";
                            if (StringUtils.isBlank(MyStringUtils.null2String(permission))) {
                                if (StringUtils.isBlank(MyStringUtils.null2String(parentPage))) {
                                    permissionNew = page;
                                } else {
                                    permissionNew = parentPage + "_" + page;
                                }
                            } else {
                                permissionNew = permission;
                            }

                            // 判断权限标识是否重复了
                            if (!"anon".equalsIgnoreCase(permissionNew) && StringUtils.isNotBlank(permissionNew)) {
                                String permissionKey = userType + "_" + lang + "_" + permissionNew;
                                if (permissionsMap.containsKey(permissionKey)) {
                                    throw new Exception("模块[" + desc + "]，权限标识[" + permissionKey + "]和已有的权限标识重复，请换一个。");
                                } else {
                                    permissionsMap.put(permissionKey, null);
                                }
                            }


                            Integer level = sysMenuParent.getLevel() + 1;
                            SysMenu menuEntity = new SysMenu();
                            menuEntity.setCode(code);
                            menuEntity.setName(name);
                            menuEntity.setDescription(desc);
                            menuEntity.setUrl(url);
                            menuEntity.setLevel(level);
                            menuEntity.setSort(Long.valueOf(order));
                            menuEntity.setImg(img);
                            menuEntity.setPcode(pcode);
                            menuEntity.setPermission(permissionNew);
                            menuEntity.setType("module");
                            menuEntity.setVisible(visible);
                            menuEntity.setIsParent("A100702");
                            menuEntity.setUserType(userType);
                            menuEntity.setLan(lang);
                            menuEntity.setCreateTime(new Date());
                            menuEntity.setIsDeleted(0);

                            parseMenuList.add(menuEntity);
                            parseMenuMap.put(key, menuEntity);
                        } else {
                            logger.info("属性code值为[" + code + "]，的节点忽略");
                        }
                    } else {
                        logger.info("已经解析过此节点：" + parseMenuMap.get(key));
                    }
                } else if (ELEMENT_OPERATION.equals(node.getNodeName())) {
                    String code = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_CODE);
                    String key = userType + "_" + lang + "_" + code;
                    if (!parseResourceMap.containsKey(key)) {  // 忽略已经解析过的节点
                        if (!"*".equals(code) && StringUtils.isNotBlank(code)) {    // id属性值为星号或为空则可以忽略
                            String name = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_NAME);
                            String desc = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_DESC);
                            String permission = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_PERMISSION);
                            String url = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_URL);
                            String requestMethod = ((org.w3c.dom.Element) node).getAttribute(ELEMENT_ATTRIBUTE_REQUESTMETHOD);
                            String menuCode = ((org.w3c.dom.Element) node.getParentNode()).getAttribute(ELEMENT_ATTRIBUTE_CODE);
                            String parentPermission = ((org.w3c.dom.Element) node.getParentNode()).getAttribute(ELEMENT_ATTRIBUTE_PERMISSION);
                            String parentPage = ((org.w3c.dom.Element) node.getParentNode()).getAttribute(ELEMENT_ATTRIBUTE_PAGE);


                            /*
                             * 权限标识=父菜单权限标识.操作按钮权限标识，
                             * 如果父菜单权限标识不填写，则使用page作为前缀，如page="user"，则权限标识为user.add
                             */
                            String permissionNew = "";
                            if ("anon".equalsIgnoreCase(parentPermission) || "anon".equalsIgnoreCase(permission)) {
                                // 父级菜单是白名单，则自己默认也是白名单
                                permissionNew = "anon";
                            } else {
                                // 父级菜单没有写权限标识，则使用page作为permission
                                if (StringUtils.isBlank(MyStringUtils.null2String(parentPermission))) {
                                    permissionNew = parentPage;
                                } else {
                                    permissionNew = parentPermission;
                                }

                                // 如果operation节点没有设置权限标识，则根据name属性值生成：Search --> search, Ab Cd-->abcd
                                if (StringUtils.isBlank(MyStringUtils.null2String(permission))) {
                                    String oPermissionNew = name.replace(" ", "");
                                    oPermissionNew = oPermissionNew.replace(oPermissionNew.substring(0, 1), oPermissionNew.substring(0, 1).toLowerCase());
                                    permissionNew = permissionNew + "." + oPermissionNew;
                                } else {
                                    permissionNew = permissionNew + "." + permission;
                                }
                            }

                            SysResource resourceEntity = new SysResource();
                            resourceEntity.setCode(code);
                            resourceEntity.setName(name);
                            resourceEntity.setDescription(desc);
                            resourceEntity.setUrl(url);
                            resourceEntity.setRequestMethod(requestMethod);
                            resourceEntity.setMenuCode(menuCode);
                            resourceEntity.setPermission(permissionNew);
                            resourceEntity.setUserType(userType);
                            resourceEntity.setLan(lang);
                            resourceEntity.setCreateTime(new Date());
                            resourceEntity.setIsDeleted(0);

                            parseResourceList.add(resourceEntity);
                            parseResourceMap.put(key, resourceEntity);
                        } else {
                            logger.info("属性code值为[" + code + "]，的节点忽略");
                        }
                    } else {
                        logger.info("已经解析过此节点：" + parseResourceMap.get(key));
                    }
                } else {
                    logger.info("未知节点：" + node.getNodeName());
                }
                doParseXML((org.w3c.dom.Element) node, db, userType, lang);
            }
        }
    }

}
