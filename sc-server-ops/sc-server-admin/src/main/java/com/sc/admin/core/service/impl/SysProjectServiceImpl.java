package com.sc.admin.core.service.impl;



import com.sc.admin.core.dao.SysProjectMapper;
import com.sc.admin.core.service.SysProjectService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.project.SysProject;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ：wust
 * @date ：Created in 2019/7/30 14:15
 * @description：
 * @version:
 */
@Service("sysProjectServiceImpl")
public class SysProjectServiceImpl extends BaseServiceImpl<SysProject> implements SysProjectService {

    @Autowired
    private SysProjectMapper sysProjectMapper;


    @Override
    protected IBaseMapper getBaseMapper() {
        return sysProjectMapper;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
