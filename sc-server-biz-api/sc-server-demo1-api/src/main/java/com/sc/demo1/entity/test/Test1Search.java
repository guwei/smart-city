package com.sc.demo1.entity.test;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-07-04 13:40:18
 * @description:
 */
public class Test1Search extends Test1 {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}