package com.sc.demo1.entity.demo;

/**
 * @author ：wust
 * @date ：Created in 2019/9/9 11:19
 * @description：
 * @version:
 */
public class DemoList extends Demo {
    private static final long serialVersionUID = -4554542040040132667L;

    private String typeLabel;
    private String statusLabel;
    private String areaTypeLabel;
    private String areaFullName;
    private Integer breakersNumber = 0;


    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getAreaTypeLabel() {
        return areaTypeLabel;
    }

    public void setAreaTypeLabel(String areaTypeLabel) {
        this.areaTypeLabel = areaTypeLabel;
    }

    public String getAreaFullName() {
        return areaFullName;
    }

    public void setAreaFullName(String areaFullName) {
        this.areaFullName = areaFullName;
    }

    public Integer getBreakersNumber() {
        return breakersNumber;
    }

    public void setBreakersNumber(Integer breakersNumber) {
        this.breakersNumber = breakersNumber;
    }

    @Override
    public String toString() {
        return super.toString() + "\nEpGatewayList{" +
                "typeLabel='" + typeLabel + '\'' +
                ", statusLabel='" + statusLabel + '\'' +
                ", areaTypeLabel='" + areaTypeLabel + '\'' +
                ", areaFullName='" + areaFullName + '\'' +
                ", breakersNumber=" + breakersNumber +
                '}';
    }
}
