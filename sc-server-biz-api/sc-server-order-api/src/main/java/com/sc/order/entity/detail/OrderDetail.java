package com.sc.order.entity.detail;

import com.sc.common.entity.BaseEntity;

import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author: wust
 * @date: 2020-07-15 14:16:53
 * @description:
 */
@Table(name = "t_detail")
public class OrderDetail extends BaseEntity {
        private Long orderId;
    private String orderNo;
    private Byte quantity;
    private Long goodsId;
    private String goodsName;
    private BigDecimal goodsPrice;
    private Long goodsPicture;
    private BigDecimal totalAmount;
    private String description;
    private Long projectId;
    private Long companyId;
                            private Integer isDeleted;


        public void setOrderId (Long orderId) {this.orderId = orderId;} 
    public Long getOrderId(){ return orderId;} 
    public void setOrderNo (String orderNo) {this.orderNo = orderNo;} 
    public String getOrderNo(){ return orderNo;} 
    public void setQuantity (Byte quantity) {this.quantity = quantity;} 
    public Byte isQuantity(){ return quantity;} 
    public void setGoodsId (Long goodsId) {this.goodsId = goodsId;} 
    public Long getGoodsId(){ return goodsId;} 
    public void setGoodsName (String goodsName) {this.goodsName = goodsName;} 
    public String getGoodsName(){ return goodsName;} 
    public void setGoodsPrice (BigDecimal goodsPrice) {this.goodsPrice = goodsPrice;} 
    public BigDecimal getGoodsPrice(){ return goodsPrice;} 
    public void setGoodsPicture (Long goodsPicture) {this.goodsPicture = goodsPicture;} 
    public Long getGoodsPicture(){ return goodsPicture;} 
    public void setTotalAmount (BigDecimal totalAmount) {this.totalAmount = totalAmount;} 
    public BigDecimal getTotalAmount(){ return totalAmount;} 
    public void setDescription (String description) {this.description = description;} 
    public String getDescription(){ return description;} 
    public void setProjectId (Long projectId) {this.projectId = projectId;} 
    public Long getProjectId(){ return projectId;} 
    public void setCompanyId (Long companyId) {this.companyId = companyId;} 
    public Long getCompanyId(){ return companyId;} 
                            public void setIsDeleted (Integer isDeleted) {this.isDeleted = isDeleted;} 
    public Integer getIsDeleted(){ return isDeleted;} 

}