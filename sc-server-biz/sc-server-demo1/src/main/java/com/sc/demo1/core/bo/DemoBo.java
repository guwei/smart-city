package com.sc.demo1.core.bo;


import com.sc.demo1.core.dao.DemoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 可复用业务对象
 */
@Component
public class DemoBo {
    @Autowired
    private DemoMapper demoGatewayMapper;
}
