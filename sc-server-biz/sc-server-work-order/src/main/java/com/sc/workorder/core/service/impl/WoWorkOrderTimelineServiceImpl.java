package com.sc.workorder.core.service.impl;

import com.sc.workorder.core.dao.WoWorkOrderTimelineMapper;
import com.sc.workorder.core.service.WoWorkOrderTimelineService;
import com.sc.workorder.entity.WoWorkOrderTimeline;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-04-09 13:13:36
 * @description:
 */
@Service("woWorkOrderTimelineServiceImpl")
public class WoWorkOrderTimelineServiceImpl extends BaseServiceImpl<WoWorkOrderTimeline> implements WoWorkOrderTimelineService {
    @Autowired
    private WoWorkOrderTimelineMapper woWorkOrderTimelineMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return woWorkOrderTimelineMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
