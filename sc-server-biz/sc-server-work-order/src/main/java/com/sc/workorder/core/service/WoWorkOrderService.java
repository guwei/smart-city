package com.sc.workorder.core.service;

import com.sc.common.service.BaseService;
import com.sc.workorder.entity.WoWorkOrder;

/**
 * @author: wust
 * @date: 2020-04-01 10:27:05
 * @description:
 */
public interface WoWorkOrderService extends BaseService<WoWorkOrder>{
    /**
     * 主动接单
     * @param id
     */
    void accept(Long id);

    /**
     * 人工派单
     * @param workOrderId
     * @param director
     */
    void distribute(Long workOrderId,Long director);

    /**
     * 完成工单
     * @param id
     */
    void finish(Long id);


    /**
     * 确认工单
     * @param workOrderId
     * @param description
     */
    void confirm(Long workOrderId,String description);
}
