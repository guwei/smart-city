package com.sc.workorder.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.workorder.entity.WoWorkOrderTimeline;

/**
 * @author: wust
 * @date: 2020-04-09 13:13:36
 * @description:
 */
public interface WoWorkOrderTimelineMapper extends IBaseMapper<WoWorkOrderTimeline>{
}