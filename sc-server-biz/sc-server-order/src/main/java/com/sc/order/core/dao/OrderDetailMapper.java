package com.sc.order.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.order.entity.detail.OrderDetail;

/**
 * @author: wust
 * @date: 2020-07-15 14:16:52
 * @description:
 */
public interface OrderDetailMapper extends IBaseMapper<OrderDetail>{
}