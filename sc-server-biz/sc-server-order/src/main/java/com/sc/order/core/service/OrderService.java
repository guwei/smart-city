package com.sc.order.core.service;

import com.sc.common.service.BaseService;
import com.sc.order.entity.order.Order;

/**
 * @author: wust
 * @date: 2020-07-15 14:14:39
 * @description:
 */
public interface OrderService extends BaseService<Order>{
}
